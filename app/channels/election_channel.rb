# frozen_string_literal: true

class ElectionChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'election_live_polls'
  end

  def unsubscribed
    # stop_all_streams
  end
end
