# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :check_authentication!
  before_action :require_authentication!

  attr_accessor :current_user, :current_session

  rescue_from Errors::Unauthorized,
              Errors::InvalidAuthenticationRequest, with: :redirect_to_login

  rescue_from ActionController::InvalidAuthenticityToken, with: :redirect_to_login

  rescue_from Errors::SessionExpired, with: :session_expired

  def check_authentication!
    @current_user ||= User.find_by(id: session[:user_id])
    @current_session ||= Session.find_by(user: current_user, ip_address: request.remote_ip)

    raise Errors::SessionExpired if current_session&.expired?

    current_user&.present?
  end

  def require_authentication!
    raise Errors::Unauthorized unless current_user.present?
  end

  def authenticate_user!(user)
    raise Errors::InvalidAuthenticationRequest if user.nil?

    @current_session ||= Session.find_by(user:, ip_address: request.remote_ip)
    return current_session unless current_session.nil?

    @current_session = Session.create(user:, ip_address: request.remote_ip)
    session[:user_id] = current_session.user.id
    session[:ip_address] = current_session.ip_address
    session[:created_at] = current_session.created_at
    session[:expires_at] = current_session.created_at + current_session.time_to_live_sec.seconds
    current_session
  end

  def destroy_secure_session!
    current_session.destroy unless current_user.nil?
    @current_user = nil
  ensure
    session.destroy
  end

  def redirect_to_login
    redirect_to new_session_path
  end

  def session_expired
    destroy_secure_session!
    redirect_to new_session_path, alert: 'Your session has expired. Please log in again.'
  end
end
