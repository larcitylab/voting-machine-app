# frozen_string_literal: true

class CandidatesController < ApplicationController
  before_action :set_candidate, only: %i[show edit update destroy]

  # GET /candidates
  def index
    @candidates = Candidate.all
  end

  # GET /candidates/1
  def show; end

  # GET /candidates/new
  def new
    @candidate = Candidate.new
  end

  # GET /candidates/1/edit
  def edit; end

  # POST /candidates
  def create
    @candidate = Candidate.new(candidate_params)
    @vote = Vote.new(candidate: @candidate, user: current_user)

    if @candidate.save && @vote.save
      redirect_to votes_path, notice: t('votes.write_in_success')
    else
      render :new, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /candidates/1
  def update
    if @candidate.update(candidate_params)
      redirect_to @candidate, notice: 'Candidate was successfully updated.', status: :see_other
    else
      render :edit, status: :unprocessable_entity
    end
  end

  # DELETE /candidates/1
  def destroy
    @candidate.destroy
    redirect_to candidates_url, notice: 'Candidate was successfully destroyed.', status: :see_other
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_candidate
    @candidate = Candidate.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def candidate_params
    params.permit(candidate: %i[full_name])[:candidate] || {}
  end
end
