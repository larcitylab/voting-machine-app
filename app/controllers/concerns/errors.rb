# frozen_string_literal: true

module Errors
  class InvalidAuthenticationRequest < StandardError; end
  class Unauthorized < StandardError; end
  class SessionExpired < StandardError; end
end
