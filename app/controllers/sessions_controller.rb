# frozen_string_literal: true

class SessionsController < ApplicationController
  before_action :set_session, only: %i[show destroy]
  after_action :destroy_secure_session!, only: %i[destroy]
  skip_before_action :require_authentication!, only: %i[new create]

  # GET /sessions
  def index
    @sessions = Session.all
  end

  # GET /sessions/1
  def show; end

  # GET /sessions/new
  def new
    if current_user.present?
      redirect_to new_vote_url, notice: t('sessions.already_logged_in')
    else
      @user = User.new(email: Faker::Internet.email, postal_code: Faker::Address.zip_code)
      @session = Session.new
    end
  end

  # DELETE /sessions/1
  def destroy
    @session&.destroy
    redirect_to new_session_path, notice: t('sessions.destroy_success'), status: :see_other
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_session
    @session = Session.find_by_id(params[:id])
    @session ||= Session.find_by(user: current_user)
  end

  # Only allow a list of trusted parameters through.
  def user_params
    params.require(:user).permit(:email, :postal_code)
  end
end
