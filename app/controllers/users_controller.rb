# frozen_string_literal: true

class UsersController < ApplicationController
  skip_before_action :check_authentication!, only: %i[create]
  skip_before_action :require_authentication!, only: %i[new create]

  before_action :set_user, only: %i[show edit update destroy]

  # GET /users
  def index
    @users = User.all
  end

  # GET /users/1
  def show; end

  # GET /users/new
  def new
    redirect_to votes_url, notice: t('sessions.already_logged_in') if current_user

    @user = User.new(email: Faker::Internet.email, postal_code: Faker::Address.zip_code)
  end

  # GET /users/1/edit
  def edit; end

  # POST /users
  def create
    email, postal_code = user_params.values_at(:email, :postal_code)
    @user = User.find_by(**user_params) if User.authenticate!(email:, postal_code:)
    @user ||= User.new(user_params)

    if @user.persisted?
      redirect_to new_vote_path, notice: t('users.already_registered')
    elsif @user.save
      redirect_to new_vote_path, notice: t('users.create_success')
    else
      render :new, status: :unprocessable_entity
    end
  ensure
    authenticate_user!(@user) if @user&.persisted?
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      redirect_to @user, notice: 'User was successfully updated.', status: :see_other
    else
      render :edit, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
    redirect_to users_url, notice: 'User was successfully destroyed.', status: :see_other
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def user_params
    params.permit(user: %i[email postal_code])[:user]
  end
end
