# frozen_string_literal: true

class VotesController < ApplicationController
  skip_before_action :require_authentication!, only: %i[index]
  before_action :set_candidate, only: %i[create]
  before_action :set_candidates, only: %i[new create]
  after_action :broadcast_ballot_tally, only: %i[create], if: -> { @vote&.persisted? }

  # GET /votes
  def index
    @votes = tallied_votes
  end

  # GET /votes/1
  def show; end

  # GET /votes/new
  def new
    existing_vote = Vote.find_by(user: current_user)
    if existing_vote
      redirect_to votes_path, notice: t('votes.ballot_already_voted')
      return
    end

    @vote = Vote.new
    @candidate = Candidate.new
  end

  # POST /votes
  def create
    # TODO: Attempt saving a candidate ONLY if there is candidate data
    @candidate = creating_candidate? ? Candidate.new(candidate_params) : Candidate.new
    @vote = Vote.new(vote_params.merge(user_id: current_user.id))

    if creating_candidate? && @candidate&.save && @vote.save
      redirect_to votes_path, notice: t('votes.write_in_success')
    elsif @vote.save
      redirect_to votes_path, notice: t('votes.ballot_success')
    else
      render :new, status: :unprocessable_entity
    end
  end

  private

  def broadcast_ballot_tally
    ActionCable.server.broadcast 'election_live_polls', { ballots_cast: total_votes }
  end

  def total_votes
    tallied_votes.sum(&:total_votes)
  end

  def tallied_votes
    @tallied_votes ||= Candidate.select('candidates.full_name, count(votes.candidate_id) as total_votes')
                                .joins(:votes).group('candidates.full_name').order('2 desc')
  end

  # Use callbacks to share common setup or constraints between actions.

  def set_candidates
    @candidates = Candidate.all
  end

  def set_candidate
    @candidate ||= Candidate.find_by_id(vote_params[:candidate_id])
    @candidate ||= Candidate.new
  end

  # Only allow a list of trusted parameters through.
  def vote_params
    params.permit(vote: %i[candidate_id user_id])[:vote] || {}
  end

  def candidate_params
    params.permit(candidate: %i[full_name])[:candidate]
  end

  def creating_candidate?
    params[:candidate].present?
  end
end
