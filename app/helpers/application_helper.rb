# frozen_string_literal: true

module ApplicationHelper
  def center_input_label_classes
    'block text-sm font-medium leading-6 text-gray-900'
  end

  def center_input_classes
    'block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6'
  end

  def center_button_classes
    'flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600'
  end

  def light_button_classes
    'text-gray-900 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2 dark:bg-gray-800 dark:text-white dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700'
  end

  def button_classes
    'rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600'
  end

  def header_text_classes(append_classes = '')
    "mt-10 #{append_classes || ''} text-2xl font-bold leading-9 tracking-tight text-gray-900"
  end

  def form_header(text, centered: true)
    content_tag :h2, text, class: header_text_classes(centered ? 'text-center' : '')
  end
end
