# frozen_string_literal: true

module SessionsHelper
  def authenticated?
    @current_user.present?
  end
end
