# frozen_string_literal: true

class ApplicationRecord < ActiveRecord::Base
  primary_abstract_class

  attribute :id, :string, default: -> { SecureRandom.uuid }
end
