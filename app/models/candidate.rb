# frozen_string_literal: true

# == Schema Information
#
# Table name: candidates
#
#  id         :string           not null, primary key
#  full_name  :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Candidate < ApplicationRecord
  BALLOT_LIMIT = 10

  has_many :votes

  validates :full_name, presence: true, uniqueness: { case_sensitive: false }
  validate :cannot_exceed_ballot_limit

  def cannot_exceed_ballot_limit
    return unless Candidate.all.count >= BALLOT_LIMIT

    errors.add(:base, "You cannot have more than #{BALLOT_LIMIT} candidates")
  end
end
