# frozen_string_literal: true

# == Schema Information
#
# Table name: sessions
#
#  id               :string           not null, primary key
#  user_id          :string           not null
#  ip_address       :string
#  time_to_live_sec :integer          not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
class Session < ApplicationRecord
  attribute :time_to_live_sec, :integer, default: 5 * 60

  belongs_to :user

  def expired?
    created_at + time_to_live_sec.seconds < Time.now
  end
end
