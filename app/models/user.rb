# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id          :string           not null, primary key
#  email       :string           not null
#  postal_code :string           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class User < ApplicationRecord
  has_many :sessions, dependent: :destroy
  has_many :votes, dependent: :destroy

  validates :email, presence: true, uniqueness: true, email: true
  validates :postal_code, presence: true

  def self.authenticate!(email:, postal_code:)
    user = find_by_email(email)
    return false unless user

    user.postal_code == postal_code
  end
end
