# frozen_string_literal: true

# == Schema Information
#
# Table name: votes
#
#  id           :string           not null, primary key
#  candidate_id :string           not null
#  user_id      :string           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
class Vote < ApplicationRecord
  belongs_to :candidate
  belongs_to :user

  validate :only_1_vote_per_user

  private

  def only_1_vote_per_user
    return if user.votes.count < 1

    errors.add(:base, I18n.t('votes.ballot_already_voted'))
  end
end
