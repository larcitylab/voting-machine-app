# frozen_string_literal: true

Rails.application.routes.draw do
  resources :candidates, only: %i[index create show]
  resources :votes, only: %i[new index create]
  resources :sessions, only: %i[new destroy] do
    collection do
      get :logout, to: 'sessions#destroy'
    end
  end
  # NOTE: Disabling the user resource since it is not used directly in the application
  resources :users, only: %i[new create]
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"

  root 'users#new'
end
