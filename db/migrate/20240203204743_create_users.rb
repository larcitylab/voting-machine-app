# frozen_string_literal: true

class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users, force: true, id: false do |t|
      t.primary_key :id, :string
      t.string :email, null: false, index: { unique: true }
      t.string :postal_code, null: false

      t.timestamps
    end
  end
end
