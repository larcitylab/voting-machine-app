# frozen_string_literal: true

class CreateVotes < ActiveRecord::Migration[7.0]
  def change
    create_table :votes, force: true, id: false do |t|
      t.primary_key :id, :string
      t.belongs_to :candidate, null: false, foreign_key: true, type: :string
      t.belongs_to :user, null: false, foreign_key: true, type: :string
      t.timestamps
    end
  end
end
