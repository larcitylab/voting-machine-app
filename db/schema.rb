# frozen_string_literal: true

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 20_240_203_212_625) do
  create_table 'candidates', id: :string, force: :cascade do |t|
    t.string 'full_name', null: false
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end

  create_table 'sessions', id: :string, force: :cascade do |t|
    t.string 'user_id', null: false
    t.string 'ip_address'
    t.integer 'time_to_live_sec', null: false
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['user_id'], name: 'index_sessions_on_user_id'
  end

  create_table 'users', id: :string, force: :cascade do |t|
    t.string 'email', null: false
    t.string 'postal_code', null: false
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['email'], name: 'index_users_on_email', unique: true
  end

  create_table 'votes', id: :string, force: :cascade do |t|
    t.string 'candidate_id', null: false
    t.string 'user_id', null: false
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['candidate_id'], name: 'index_votes_on_candidate_id'
    t.index ['user_id'], name: 'index_votes_on_user_id'
  end

  add_foreign_key 'sessions', 'users'
  add_foreign_key 'votes', 'candidates'
  add_foreign_key 'votes', 'users'
end
