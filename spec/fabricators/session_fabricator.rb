# frozen_string_literal: true

# == Schema Information
#
# Table name: sessions
#
#  id               :string           not null, primary key
#  user_id          :string           not null
#  ip_address       :string
#  time_to_live_sec :integer          not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
Fabricator(:session) do
  time_to_live_sec { 60 * 5 }
  ip_address       { Faker::Internet.ip_v4_address }
end
