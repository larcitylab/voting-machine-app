# frozen_string_literal: true

require 'rails_helper'
require 'active_support/testing/time_helpers'

describe 'Voting', type: :feature do
  include ActiveSupport::Testing::TimeHelpers

  before(:all) do
    self.accessibility_audit_enabled = true
  end

  let(:email) { Faker::Internet.email }
  let(:postal_code) { Faker::Address.postcode }

  context 'with a candidate on the ballot' do
    before do
      Fabricate :candidate, full_name: 'John Doe'
      Fabricate :candidate, full_name: 'Jane Saunders'
      Fabricate :candidate, full_name: 'Paula Smith'
    end

    context 'and an unregistered voter' do
      let(:candidate) { Candidate.find_by(full_name: 'Jane Saunders') }

      it 'saves their vote successfully' do
        visit '/sessions/new'

        with_accessibility_audit_options according_to: :wcag21aaa do
          fill_in 'Email', with: email
          fill_in 'Postal code', with: postal_code
          click_on 'Continue to vote'
        end

        expect(page).to have_content("Signed in as #{email}")
        expect(page).to have_current_path('/votes/new')
        expect(page).to have_content('Cast your vote today!')

        with_accessibility_audit_options according_to: :wcag21aaa do
          choose option: candidate.id
          expect { click_on 'Vote' }.to change { Vote.count }.by(1)
        end

        expect(page).to have_content('Your vote has been successfully submitted')
      end
    end
  end

  context 'with a write-in candidate' do
    context 'and an unregistered voter' do
      it 'saves their vote successfully' do
        visit '/sessions/new'

        with_accessibility_audit_options according_to: :wcag21aaa do
          fill_in 'Email', with: email
          fill_in 'Postal code', with: postal_code
          click_on 'Continue to vote'
        end

        expect(page).to have_content("Signed in as #{email}")
        expect(page).to have_current_path('/votes/new')
        expect(page).to have_content('Cast your vote today!')

        with_accessibility_audit_options according_to: :wcag21aaa do
          fill_in 'candidate_full_name', with: 'John Doe'
          expect { click_on 'Write in and vote' }.to change { Vote.count }.by(1).and change { Candidate.count }.by(1)
        end

        expect(page).to have_content('Your vote for your write-in candidate has been successfully submitted')
      end
    end

    context 'and a registered voter' do
      before do
        Fabricate :user, email:, postal_code:
      end

      it 'saves their vote successfully' do
        visit '/sessions/new'

        with_accessibility_audit_options according_to: :wcag21aaa do
          fill_in 'Email', with: email
          fill_in 'Postal code', with: postal_code
          click_on 'Continue to vote'
        end

        expect(page).to have_content("Signed in as #{email}")
        expect(page).to have_current_path('/votes/new')
        expect(page).to have_content('Cast your vote today!')

        with_accessibility_audit_options according_to: :wcag21aaa do
          fill_in 'candidate_full_name', with: 'Jeremy Nix'
          expect { click_on 'Write in and vote' }.to change { Vote.count }.by(1).and change { Candidate.count }.by(1)
        end

        expect(page).to have_content('Your vote for your write-in candidate has been successfully submitted')
      end
    end
  end
end
