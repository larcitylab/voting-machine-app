# frozen_string_literal: true

require 'rails_helper'

describe.skip 'candidates/new', type: :view do
  before(:each) do
    assign(:candidate, Candidate.new(
                         full_name: 'MyString'
                       ))
  end

  it 'renders new candidate form' do
    render

    assert_select 'form[action=?][method=?]', candidates_path, 'post' do
      assert_select 'input[name=?]', 'candidate[full_name]'
    end
  end
end
